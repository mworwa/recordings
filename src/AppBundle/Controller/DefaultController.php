<?php

namespace AppBundle\Controller;

use Ddeboer\Imap\Exception\Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Manager\ImapManager;
use Ddeboer\Imap\MessageIterator;



class DefaultController extends Controller
{
    /**
     * @Route("/get/recordings", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $token = 'vyrcgv4whrcgq3evrhcqwrhvcegrwfdacrdfgxfd';
        if($request->headers->get('authorization') !== $token){
            throw $this->createAccessDeniedException('Token not valid.');
        }
        $overallAttachments = [];
        $properties = $request->query->all();

        $imapManager = new ImapManager();
        $attachmentManager = $this->get('app.attachment_manager');


        $server = $imapManager->createServer($properties['server']);
        $connection = $imapManager->getConntection($server, $properties['username'], $properties['password']);

        $mailbox = $connection->getMailbox('INBOX');
        $search = $imapManager->createSearchExpression($properties['phone']);
        $messages = $imapManager->getMessages($search, $mailbox);

        if ($messages->count() == 0) {
            return new JsonResponse([]);
       }

        foreach ($messages as $message) {
            $storedAttachmentsArray = $attachmentManager->insertStoredAttachmentsIntoArray();
            $attachments = $message->getAttachments();
            if (!$attachments) {
                return new JsonResponse([]);
            }
            foreach ($attachments as $attachment) {
                $overallAttachments[] = $attachment;
                $attachmentName = $attachment->getFilename();
                if (!array_key_exists($attachmentName, $storedAttachmentsArray) && $properties['order'] == 1) {
                    $decodedAttachment = $attachment->getDecodedContent();
                    $attachmentManager->saveAttachment($attachmentName, base64_encode($decodedAttachment));
                }
            }

        }
            $attachmentJson = $attachmentManager->prepareRecordingsJson($overallAttachments);
            return $attachmentJson;

    }
}
