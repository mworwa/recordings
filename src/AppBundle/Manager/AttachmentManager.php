<?php
/**
 * Created by PhpStorm.
 * User: marcinworwa
 * Date: 17.03.2017
 * Time: 09:24
 */

namespace AppBundle\Manager;



use Symfony\Component\HttpFoundation\JsonResponse;

class AttachmentManager
{
    const DIR = '/var/www/html/recordings/web/recordings/';

    /**
     * @param $filename
     * @param $decodedAttachment
     */
    public function saveAttachment($filename, $decodedAttachment)
    {
        file_put_contents(
            self::DIR . $filename,
            $decodedAttachment
        );
    }

    /**
     * @return array
     */
    public function insertStoredAttachmentsIntoArray()
    {
        $files = scandir(self::DIR);
        return $files;

    }

    /**
     * @param Attachment $attachments
     */
    public function prepareRecordingsJson($attachments){
        $json = [];
        foreach($attachments as $attachment) {
            $decodedAttachment = $attachment->getDecodedContent();
            $json[] =
                array(
                    'file' => base64_encode($decodedAttachment)
                );
        }
        return new JsonResponse($json);
    }
}