<?php
namespace AppBundle\Manager;

use Ddeboer\Imap\Mailbox;
use Ddeboer\Imap\Server;
use Ddeboer\Imap\SearchExpression;
use Ddeboer\Imap\Search\Text\Subject;
use Ddeboer\Imap\MessageIterator;


class ImapManager {

    /**
     * @param $server
     * @return Server
     */
    public function createServer(string $server) {
        $server = new Server($server);
        return $server;
    }

    /**
     * @param $server
     * @param $username
     * @param $password
     * @return mixed
     */

    public function getConntection(Server $server, string $username, string $password ) {
        $connection = $server->authenticate($username, $password);
        return $connection;
    }

    public function getMailboxes($connection) {

        $mailboxes = $connection->getMailboxes();

        foreach ($mailboxes as $mailbox) {
            printf('Mailbox %s has %s messages', $mailbox->getName(), $mailbox->count());
        }
    }

    public function createSearchExpression($phone){
        $search = new SearchExpression();
        $search->addCondition(new Subject($phone));
        return $search;
    }

    /**
     * @param SearchExpression $search
     * @param Mailbox $mailbox
     * @return \Ddeboer\Imap\Message[]|\Ddeboer\Imap\MessageIterator
     */
    public function getMessages(SearchExpression $search, Mailbox $mailbox ) {
        $message = $mailbox->getMessages($search);
        return $message;
    }








}